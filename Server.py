import os
import random
from flask import Flask, redirect, request, render_template, jsonify, make_response, escape, session
from flask_mail import Mail, Message
import sqlite3
from werkzeug.utils import secure_filename
import pdfcrowd

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
UPLOAD_CV_FOLDER = os.path.join(APP_ROOT, 'cv')
UPLOAD_PHOTO_FOLDER = os.path.join(APP_ROOT, 'photos')
ALLOWED_EXTENSIONS = set(['jpg','jpeg','png','gif','pdf'])
DATABASE = "acornclients.db"

app = Flask(__name__)
app.config['UPLOAD_CV_FOLDER'] = UPLOAD_CV_FOLDER
app.config['UPLOAD_PHOTO_FOLDER'] = UPLOAD_PHOTO_FOLDER
app.config.update(
    MAIL_SERVER='smtp.gmail.com',
    MAIL_PORT=465,
    MAIL_USE_SSL=True,
    MAIL_USERNAME = 'acornemailsend@gmail.com',
    MAIL_PASSWORD = 'AcornProject'
)
#Adapted from https://stackoverflow.com/questions/37058567/configure-flask-mail-to-use-gmail

mail = Mail(app)


def allowed_file(filename):
    ext = filename.rsplit('.', 1)[1].lower()
    print(ext)
    return '.' in filename and ext in ALLOWED_EXTENSIONS

@app.route("/home", methods=['GET'])
def home():
    if request.method == 'GET':
        # user_type = ("None set")
        userid = request.cookies.get("username")
        usertype = request.cookies.get("usertype")

        print(usertype)
        return render_template("general.html", user_id = userid, user_type = usertype)


@app.route("/admin", methods=['GET'])
def admin():
    if request.method == 'GET':
        userid = request.cookies.get("username")
        usertype = request.cookies.get("usertype")
        if usertype == ("admin") or usertype == ("staff"):
            return render_template("admin.html", user_id = userid, user_type = usertype)
        else:
            return render_template("general.html", user_id=userid, user_type=usertype)

@app.route("/adminViewDetails", methods=['GET'])
def adminViewDetails():
    if request.method == 'GET':
        userid = request.cookies.get("username")
        usertype = request.cookies.get("usertype")
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("SELECT * FROM client_details;")
        alldata = cur.fetchall()

        if usertype == ("admin") or usertype==("staff"):
            return render_template("admin_details.html", user_id = userid, user_type = usertype, data = alldata)
        else:
            return render_template("general.html", user_id=userid, user_type=usertype)

@app.route("/adminViewAddress", methods=['GET'])
def adminViewAddress():
    if request.method == 'GET':
        userid = request.cookies.get("username")
        usertype = request.cookies.get("usertype")
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("SELECT * FROM client_address;")
        alldata = cur.fetchall()

        if usertype == ("admin") or usertype==("staff"):
            return render_template("admin_address.html", user_id = userid, user_type = usertype, data = alldata)
        else:
            return render_template("general.html", user_id=userid, user_type=usertype)

@app.route("/adminViewReferences", methods=['GET'])
def adminViewReferences():
    if request.method == 'GET':
        userid = request.cookies.get("username")
        usertype = request.cookies.get("usertype")
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("SELECT * FROM client_references;")
        alldata = cur.fetchall()

        if usertype == ("admin") or usertype==("staff"):
            return render_template("admin_references.html", user_id = userid, user_type = usertype, data = alldata)
        else:
            return render_template("general.html", user_id=userid, user_type=usertype)

@app.route("/adminViewWork", methods=['GET'])
def adminViewWork():
    if request.method == 'GET':
        userid = request.cookies.get("username")
        usertype = request.cookies.get("usertype")
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("SELECT * FROM client_work;")
        alldata = cur.fetchall()

        if usertype == ("admin") or usertype==("staff"):
            return render_template("admin_work.html", user_id = userid, user_type = usertype, data = alldata)
        else:
            return render_template("general.html", user_id=userid, user_type=usertype)

@app.route("/adminViewDocuments", methods=['GET'])
def adminViewDocuments():
    if request.method == 'GET':
        userid = request.cookies.get("username")
        usertype = request.cookies.get("usertype")
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("SELECT * FROM client_documents;")
        alldata = cur.fetchall()

        if usertype == ("admin") or usertype==("staff"):
            return render_template("admin_documents.html", user_id = userid, user_type = usertype, data = alldata)
        else:
            return render_template("general.html", user_id=userid, user_type=usertype)

@app.route("/adminViewAll", methods=['GET', 'POST'])
def adminViewAll():
    if request.method == 'GET':
        userid = request.cookies.get("username")
        usertype = request.cookies.get("usertype")
        return render_template("admin_all.html", user_id = userid, user_type = usertype)

    if request.method == 'POST':
        userid = request.cookies.get("username")
        usertype = request.cookies.get("usertype")
        request_id =  request.form.get('requestid', default="Error")
        #
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("SELECT * FROM client_details WHERE ID=?;",[request_id])
        alldetail = cur.fetchall()
        #
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("SELECT * FROM client_address WHERE ID=?;",[request_id])
        alladdress = cur.fetchall()
        #
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("SELECT * FROM client_work WHERE userID=?;",[request_id])
        allUkDetails = cur.fetchall()
        #
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("SELECT * FROM client_references WHERE UserIDLink=?;",[request_id])
        allreferences = cur.fetchall()
        #
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("SELECT * FROM client_documents WHERE UserID=?;",[request_id])
        alldocuments = cur.fetchall()
        #
        if usertype == ("admin") or usertype==("staff"):
            return render_template("admin_all.html", user_id = userid, user_type = usertype, detail=alldetail, address=alladdress, ukDetails=allUkDetails, references=allreferences, documents=alldocuments)
        else:
            return render_template("general.html", user_id=userid, user_type=usertype)

@app.route("/sendForm", methods=['POST'])
def sendForm():

    if request.method == 'POST':

        request_id =  request.form["request_id"]
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("SELECT count(1) FROM client_details WHERE ID=?;", [request_id])
        check_exist = cur.fetchall()[0][0]
        print(check_exist)
        if check_exist == (0):
            return("Failed, check client exists")

        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("SELECT * FROM client_details WHERE ID=?;",[request_id])
        alldetail = cur.fetchall()
        #
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("SELECT * FROM client_address WHERE ID=?;",[request_id])
        alladdress = cur.fetchall()
        #
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("SELECT * FROM client_work WHERE userID=?;",[request_id])
        allUkDetails = cur.fetchall()
        #
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("SELECT * FROM client_references WHERE UserIDLink=?;",[request_id])
        allreferences = cur.fetchall()
        #
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("SELECT * FROM client_documents WHERE UserID=?;",[request_id])
        alldocuments = cur.fetchall()
        rendered_document = render_template("admin_pdf.html", detail=alldetail, address=alladdress, ukDetails=allUkDetails, references=allreferences, documents=alldocuments)
        # try:
        #Adapted from https://pythonhosted.org/Flask-Mail/
        # create an API client instance
        client = pdfcrowd.Client("AcornWebTest", "327e4f4eb27b48e91c556f808e6c1c9d")
        # # convert an HTML string and save the result to a file
        output_file = open('pdf\\user_'+str(request_id)+'.pdf', 'wb')
        html=rendered_document
        client.convertHtml(html, output_file)
        output_file.close()
        msg = Message("Confirmed Client "+str(request_id),
                        sender='acornemailsend@gmail.com',
                        recipients=['lightjp@cardiff.ac.uk'])



        with app.open_resource('cv\\'+str(request_id)+'_CV'+'.pdf') as fp:
            msg.attach('cv\\'+str(request_id)+'_CV'+'.pdf', 'cv\\'+str(request_id)+'_CV'+'/pdf', fp.read())
            fp.close()
        print("test1")
        with app.open_resource('photos\\'+str(request_id)+'_PHOTO'+'.png') as fp1:
            print(fp1)
            msg.attach('photos\\'+str(request_id)+'_PHOTO'+'.png', 'photos\\'+str(request_id)+'_PHOTO'+'/png', fp1.read())
        print("test")
        with app.open_resource('pdf\\user_'+str(request_id)+'.pdf') as fp2:
            msg.attach('pdf\\user_'+str(request_id)+'.pdf', 'pdf\\user_'+str(request_id)+'/pdf', fp2.read())

        mail.send(msg)

        # finally:
        return "Client confirmed and sent"


@app.route("/adminDelete", methods=['GET', 'POST'])
def adminDelete():
    if request.method == 'GET':
        userid = request.cookies.get("username")
        usertype = request.cookies.get("usertype")
        if usertype == ("admin"):
            return render_template("admin_delete.html", user_id = userid, user_type = usertype)
        else:
            return render_template("general.html", user_id=userid, user_type=usertype)

    if request.method == 'POST':
        userid = request.cookies.get("username")
        usertype = request.cookies.get("usertype")
        request_id =  request.form.get('requestid', default="Error")
        #
        try:
            conn = sqlite3.connect(DATABASE)
            cur = conn.cursor()
            cur.execute("DELETE FROM client_details WHERE ID=?;",[request_id])
            cur.execute("DELETE FROM client_address WHERE ID=?;",[request_id])
            cur.execute("DELETE FROM client_documents WHERE UserID=?;",[request_id])
            cur.execute("DELETE FROM client_references WHERE UserIDLink=?;",[request_id])
            cur.execute("DELETE FROM client_work WHERE userID=?;",[request_id])
            cur.execute("DELETE FROM user_logins WHERE User_ID=?;",[request_id])
            conn.commit()
            msg = "User has been removed"
        except:
            conn.rollback()
            msg = ("User removal unsuccesful")
        finally:
            conn.close()
            if usertype == ("admin"):
                return render_template("admin_delete.html", user_id = userid, user_type = usertype, message=msg)
            else:
                return render_template("general.html", user_id=userid, user_type=usertype)


        #



@app.route("/adminLogins", methods=['GET', 'POST'])
def adminLogins():
    if request.method == 'GET':
        userid = request.cookies.get("username")
        usertype = request.cookies.get("usertype")
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("SELECT User_ID, username, useremail, usertype FROM user_logins;")
        alldata = cur.fetchall()
        conn.close()
        if usertype == ("admin") or usertype==("staff"):

            return render_template("admin_logins.html", user_id = userid, user_type = usertype, data = alldata)
        else:
            return render_template("general.html", user_id=userid, user_type=usertype)

    if request.method == 'POST':
        userid = request.cookies.get("username")
        usertype = request.cookies.get("usertype")

        changeUserID = request.form.get("user_id")
        actUserID = request.cookies.get("userid")
        changeUserType = request.form.get("newType")

        if actUserID == changeUserID:
            conn = sqlite3.connect(DATABASE)
            cur = conn.cursor()
            cur.execute("SELECT User_ID, username, useremail, usertype FROM user_logins;")
            alldata = cur.fetchall()
            conn.close()
            return render_template("admin_logins.html", user_id = userid, user_type = usertype, data = alldata)

        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("SELECT User_ID FROM user_logins;")
        alldata = cur.fetchall()
        conn.close()

        userid_check = ("("+ changeUserID +",)")

        for i in alldata:
            if str(i) == str(userid_check):
                conn = sqlite3.connect(DATABASE)
                cur = conn.cursor()


                cur.execute("UPDATE user_logins SET usertype = ? WHERE user_ID = ? ",[changeUserType, changeUserID])
                conn.commit()
                conn.close()
                conn = sqlite3.connect(DATABASE)
                cur = conn.cursor()
                cur.execute("SELECT User_ID, username, useremail, usertype FROM user_logins;")
                alldata = cur.fetchall()
                conn.close()
                return render_template("admin_logins.html", user_id = userid, user_type = usertype, data = alldata)

        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("SELECT User_ID, username, useremail, usertype FROM user_logins;")
        alldata = cur.fetchall()
        conn.close()
        return render_template("admin_logins.html", user_id = userid, user_type = usertype, data = alldata)

@app.route("/makeUser", methods=['POST'])
def makeUser():
    if request.method == 'POST':
        userid = request.form.get("user_id")
        print(userid)

@app.route("/view", methods=['GET'])
def view():
    if request.method == 'GET':
        userid = request.cookies.get("username")
        usertype = request.cookies.get("usertype")
        request_id = request.cookies.get("userid")
        #
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("SELECT * FROM client_details WHERE ID=?;",[request_id])
        alldetail = cur.fetchall()
        #
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("SELECT * FROM client_address WHERE ID=?;",[request_id])
        alladdress = cur.fetchall()
        #
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("SELECT * FROM client_work WHERE userID=?;",[request_id])
        allUkDetails = cur.fetchall()
        #
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("SELECT * FROM client_references WHERE UserIDLink=?;",[request_id])
        allreferences = cur.fetchall()
        #
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("SELECT * FROM client_documents WHERE UserID=?;",[request_id])
        alldocuments = cur.fetchall()
        #

        return render_template("user_all.html", user_id = userid, user_type = usertype, detail=alldetail, address=alladdress, ukDetails=allUkDetails, references=allreferences, documents=alldocuments)



        return render_template("user_all.html", user_id = userid, user_type = usertype)

@app.route("/login", methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        userid = request.cookies.get("username")
        usertype = request.cookies.get("usertype")
        return render_template("login.html", user_id = userid, user_type = usertype, msgLogin="")
    if request.method =='POST':
        try:
            useremail = request.form.get('useremail', default="Error")
            userpassword = request.form.get('userpassword', default="Error")
            conn = sqlite3.connect(DATABASE)
            cur = conn.cursor()
            cur.execute("SELECT * FROM user_logins WHERE useremail=? AND userpassword=?;", [useremail, userpassword])
            tupleUserInfo = cur.fetchone()
            user_unique_id = tupleUserInfo[0]
            user_name = tupleUserInfo[2]
            user_type = tupleUserInfo[4]
            # print(tupleUserInfo)
            # print(user_unique_id)
            # print(user_name)
            # print(user_type)
            # print(useremail)
            # print(userpassword)

            if len(tupleUserInfo)>0:

                 resp = make_response(render_template("login.html", msgLogin="Succesful, please go to home page"))
                 resp.set_cookie("username", user_name)
                 resp.set_cookie("usertype", user_type)

                 resp.set_cookie("userid", str(user_unique_id))
            else:
                resp = make_response(render_template("login.html", msgLogin="Username or password incorrect"))
        except:
            print('An error has occured')
            resp = make_response(render_template("login.html", msgLogin="An error has occured"))
        finally:
            conn.close()
            return resp

@app.route("/signout", methods=['GET'])
def signout():
    resp = make_response(render_template("general.html", user_id=("Logged out"), user_type=("None")))
    resp.set_cookie('username', '', expires=0)
    resp.set_cookie('usertype', '', expires=0)
    resp.set_cookie('userid', '', expires=0)
    return resp


@app.route("/signup", methods=['GET' , 'POST'])
def signup():
    if request.method == 'GET':
        userid = request.cookies.get("username")
        usertype = request.cookies.get("usertype")

        return render_template("signup.html", userid = userid, user_type = usertype)
    if request.method == 'POST':
        user_name = request.form.get("user_name", default = "Error")
        user_email = request.form.get("user_email", default = "Error")
        user_password = request.form.get("user_password", default = "Error")
        user_type = "user"

        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("SELECT useremail FROM user_logins;")
        allemails = cur.fetchall()

        useremail_check = ("('"+ user_email +"',)")
        # print(useremail_check)
        for i in allemails:
            if str(i) == str(useremail_check):
                msg = "Sorry that email is already used"
                return render_template("login.html",  message = msg)


        try:
            conn = sqlite3.connect(DATABASE)
            cur = conn.cursor()
            cur.execute("INSERT INTO user_logins ('username', 'useremail', 'userpassword', 'usertype')\
                         VALUES (?,?,?,?)",(user_name, user_email, user_password, user_type) )
            conn.commit()
            #Getting unique user id
            conn_id = sqlite3.connect(DATABASE)
            cur_id = conn_id.cursor()
            cur_id.execute("SELECT User_ID FROM user_logins WHERE useremail=?;",[user_email])
            user_unique_id = str(cur_id.fetchone()[0])

            # # ##
            msg = "User successfully created"
            resp = make_response(render_template("form_complete.html", message = msg))

            print(user_unique_id)



            resp.set_cookie("username", user_name)
            resp.set_cookie("usertype", user_type)
            resp.set_cookie("userid", user_unique_id)



        except:
            conn.rollback()
            msg = "Unfortunatly there has been an unexpected error, please try again"
        finally:
            conn.close()
            return resp


@app.route("/form", methods=['GET', 'POST'])
def returnForm():

    if request.method == 'GET':
        userid = request.cookies.get("username")
        usertype = request.cookies.get("usertype")
        return render_template('form.html', user_id = userid, user_type = usertype)

    if request.method == 'POST':
        userid = request.cookies.get("username")
        usertype = request.cookies.get("usertype")
        user_id = request.cookies.get("userid")

        firstname = request.form.get("clientName", default = "Error")
        surname = request.form.get("surname", default = "Error")
        address1 = request.form.get("address1", default = "Error")
        address2 = request.form.get("address2", default = "Error")
        addressCity = request.form.get("addressCity", default = "Error")
        addressCountry = request.form.get("addressCountry", default = "Error")
        addressPostCode = request.form.get("addressPostCode", default = "Error")
        contactNo = request.form.get("contactNo", default = "Error")
        contactEmail = request.form.get("clientEmail", default = "Error")
        emgContactName = request.form.get("emergencyContactName", default = "Error")
        emgContactNo = request.form.get("emergencyContactNo", default = "Error")
        typeWork = request.form.get("typeOfWork", default = "Error")
        qualifications = request.form.get("qualification", default = "Error")
        nationalInsurance = request.form.get("ni", default = "Error")
        dateOfBirth = request.form.get("dob", default = "Error")
        workingVia = request.form.get("workingVia", default = "Error")
        workingViaName = request.form.get("nameOfComapny", default = "Error")
        workInUK = request.form.get("workInUK", default = "Error")
        drivingLicense = request.form.get("license", default = "Error")
        criminalConvictions = request.form.get("convictions", default = "Error")
        criminalConvictionsDetails = request.form.get("convTextArea", default = "Error")
        disability = request.form.get("disability", default = "Error")
        disabilityDetails = request.form.get("disaTextArea", default = "Error")
        ref1Name = request.form.get("ref1Name", default = "Error")
        ref1JobTitle = request.form.get("ref1Title", default = "Error")
        ref1Company = request.form.get("ref1Company", default = "Error")
        ref1Address = request.form.get("ref1Address", default = "Error")
        ref1Number = request.form.get("ref1Number", default = "Error")
        ref1Email = request.form.get("ref1Email", default = "Error")
        ref2Name = request.form.get("ref2Name", default = "Error")
        ref2JobTitle = request.form.get("ref2Title", default = "Error")
        ref2Company = request.form.get("ref2Company", default = "Error")
        ref2Address = request.form.get("ref2Address", default = "Error")
        ref2Number = request.form.get("ref2Number", default = "Error")
        ref2Email = request.form.get("ref2Email", default = "Error")
        dataURL = request.form.get("dataURL", default = "Error")
        print(dataURL)
        print(request.files)
        try:
            conn = sqlite3.connect(DATABASE)
            cur = conn.cursor()
            print(userid)
            cur.execute("SELECT User_ID FROM user_logins WHERE username=?;",userid)
            idNum = str(cur.fetchone()[0])
            print("Get user numerical id success")
        except:
            idNum = "Error"
            print("Get user numerical id error")
        finally:
            print("Get num id done")
            conn.close()
        if 'cv' not in request.files or request.files['cv'].filename == '':
            hasCV = "No"
            cvPath = "None"
        else:
            hasCV = "Yes"
            cv = request.files['cv']
            if cv and allowed_file(cv.filename):
                cv_filename = secure_filename(cv.filename)
                cvExt = cv_filename.rsplit('.', 1)[1].lower()
                if userid != None:
                    # filename = secure_filename("CV_{}.{}" .format(user_id, cvExt))
                    filename = secure_filename(str(user_id)+"_CV."+cvExt)
                else:
                    filename = secure_filename("cv_name_{}_{}.{}" .format(firstname, surname, cvExt))
                filePath = os.path.join(app.config['UPLOAD_CV_FOLDER'], filename)
                cv.save(filePath)
                cvPath = ("cv/"+filename)
            else:
                print("21")
                hasCV = "No"
                cvPath = "None"
        if 'photo' not in request.files or request.files['photo'].filename == '':
            hasPhoto = "No"
            photoPath = "None"
        else:
            hasPhoto = "Yes"
            photo = request.files['photo']
            photo_filename = secure_filename(photo.filename)
            photoExt = photo_filename.rsplit('.', 1)[1].lower()
            if photo and allowed_file(photo.filename):
                if userid != None:
                    # filename = secure_filename("PHOTO_{}.{}" .format(idNum, photoExt))
                    filename = secure_filename(str(user_id)+"_PHOTO."+photoExt)
                else:
                    filename = secure_filename("photo_name_{}_{}.{}" .format(firstname, surname, photoExt))
                filePath = os.path.join(app.config['UPLOAD_PHOTO_FOLDER'], filename)
                photo.save(filePath)
                photoPath = ("photos/"+filename)
            else:
                hasPhoto = "No"
                photoPath = "None"
        print(cvPath)
        print(photoPath)
        try:


            conn = sqlite3.connect(DATABASE)
            cur = conn.cursor()
            cur.execute("INSERT INTO client_details ('ID', 'firstname', 'surname',\
                        'ContactNo', 'ContactEmail', 'EmergencyContactName', 'EmergencyContactNo',\
                        'Ni', 'DateBirth')\
                        VALUES (?,?,?,?,?,?,?,?,?)", (user_id, firstname, surname, contactNo,\
                        contactEmail, emgContactName, emgContactNo, nationalInsurance, dateOfBirth))

            cur.execute("INSERT INTO client_address ('ID', 'address1', 'address2', 'addressCity',\
                        'addressCountry', 'addressPostCode') VALUES (?,?,?,?,?,?)", (user_id, address1,\
                        address2, addressCity, addressCountry, addressPostCode))

            cur.execute("INSERT INTO client_work ('UserID', 'qualification', 'workVia', 'workViaName', 'workUk',\
                        'licenseUk', 'criminalConv', 'criminalConvDetails', 'disability',\
                        'disabilityDetails') VALUES (?,?,?,?,?,?,?,?,?,?)", (user_id, qualifications, workingVia,\
                        workingViaName, workInUK, drivingLicense, criminalConvictions, criminalConvictionsDetails,\
                        disability, disabilityDetails))

            cur.execute("INSERT INTO client_references ('UserIDLink','Name', 'JobTitle', 'Company', 'Address', 'Number',\
                        'Email') VALUES (?,?,?,?,?,?,?)", (user_id, ref1Name, ref1JobTitle, ref1Company, ref1Address,\
                        ref1Number, ref1Email))
            cur.execute("INSERT INTO client_references ('UserIDLink','Name', 'JobTitle', 'Company', 'Address', 'Number',\
                        'Email') VALUES (?,?,?,?,?,?,?)", (user_id, ref2Name, ref2JobTitle, ref2Company, ref2Address,\
                        ref2Number, ref2Email))
            cur.execute("INSERT INTO client_documents ('UserID', 'cvPath', 'photoPath')\
                        VALUES (?,?,?)", (user_id, cvPath, photoPath))
            conn.commit()
            msg="Details successfully added"

        except:
            conn.rollback()
            msg = "Unfortunatly there has been an unexpected error, please try again"
        finally:
            conn.close()




        return render_template('form_complete.html',  message = msg, user_id = userid, user_type = usertype)

@app.route("/SW", methods = ['GET'])
def serviceWorker():
	return app.send_static_file('resources/sw.js')








if __name__ == "__main__":
    # app.run(debug=True)
    app.run(host='0.0.0.0', debug=True)
