// function resendStoredData(){
//   var msg = "null"
//   var myStorage = window.localStorage;
//   messages = [];
//   console.log(messages.length);
//   for (message in myStorage){
//     var messageObj = JSON.parse(myStorage.getItem(message));
//     messages.push(messageObj);
//   }
//   myStorage.clear()
//   console.log(messages);
//   for (i=0;i< messages.length;i++){
//     if (messages[i] != null){
//       console.log(messages[i]);
//       ajaxData(messages[i]["action"], messages[i]["method"], messages[i]["params"]);
//     }
//   }
// }

function ajaxData(action, method, params){
  var xhttp = new XMLHttpRequest();
  xhttp.open(method, action, true); // true is asynchronous
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.onreadystatechange = function() {
    var msg = null;
    if (xhttp.readyState === 4){
      if (xhttp.status === 200) {
        console.log(xhttp.responseText);
        msg = xhttp.responseText;
      } else if (xhttp.status === 503 || xhttp.status === 0 ) {
        console.error(xhttp.statusText);
        offlineStore("/Student/AddStudent","POST", params);
        msg = "browser off line data stored for later use "+xhttp.status;
      } else {
        console.error(xhttp.statusText);
        msg = "other wierd error "+xhttp.status;
      }
      document.getElementById("txt").innerHTML = msg+"<br>"+document.getElementById("txt").innerHTML;
    }
  };
  xhttp.send(params);
}

function offlineStore(action, method, params){
  myStorage = window.localStorage;
  storageItem = JSON.stringify({"action":action, "method":method, "params":params});
  myStorage.setItem('Student'+myStorage.length, storageItem);
}

function loadName() {
    var sigCanvas = document.getElementById("canvas");
    var context = sigCanvas.getContext('2d');
    context.putImageData(...);
    var dataURL = scratchCanvas.toDataURL();
  var clientName = document.forms["myForm"]["clientName"].value;
  var surname = document.forms["myForm"]["surname"].value;
  var address1 = document.forms["myForm"]["address1"].value;
  var address2 = document.forms["myForm"]["address2"].value;
  var addressCity = document.forms["myForm"]["addressCity"].value;
  var addressCountry = document.forms["myForm"]["addressCountry"].value;
  var addressPostCode = document.forms["myForm"]["addressPostCode"].value;
  var contactNo = document.forms["myForm"]["contactNo"].value;
  var clientEmail = document.forms["myForm"]["clientEmail"].value;
  var emergencyContactName = document.forms["myForm"]["emergencyContactName"].value;
  var emergencyContactNo = document.forms["myForm"]["emergencyContactNo"].value;
  var typeOfWork = document.forms["myForm"]["typeOfWork"].value;
  var qualification = document.forms["myForm"]["qualification"].value;
  var ni = document.forms["myForm"]["ni"].value;
  var dob = document.forms["myForm"]["dob"].value;
  var workingVia = document.forms["myForm"]["workingVia"].value;
  var nameOfComapny = document.forms["myForm"]["nameOfComapny"].value;
  var workInUK = document.forms["myForm"]["workInUK"].value;
  var license = document.forms["myForm"]["license"].value;
  var convictions = document.forms["myForm"]["convictions"].value;
  var convTextArea = document.forms["myForm"]["convTextArea"].value;
  var disability = document.forms["myForm"]["disability"].value;
  var disaTextArea = document.forms["myForm"]["disaTextArea"].value;
  var ref1Name = document.forms["myForm"]["ref1Name"].value;
  var ref1Title = document.forms["myForm"]["ref1Title"].value;
  var ref1Company = document.forms["myForm"]["ref1Company"].value;
  var ref1Address = document.forms["myForm"]["ref1Address"].value;
  var ref1Number = document.forms["myForm"]["ref1Number"].value;
  var ref1Email = document.forms["myForm"]["ref1Email"].value;
  var ref2Name = document.forms["myForm"]["ref2Name"].value;
  var ref2Title = document.forms["myForm"]["ref2Title"].value;
  var ref2Company = document.forms["myForm"]["ref2Company"].value;
  var ref2Address = document.forms["myForm"]["ref2Address"].value;
  var ref2Number = document.forms["myForm"]["ref2Number"].value;
  var ref2Email = document.forms["myForm"]["ref2Email"].value;
  params ='clientName='+clientName+'&surname='+surname+'&address1='+address1+
  '&address2='+address2+'&addressCity='+addressCity+
  '&addressCountry='+addressCountry+'&addressPostCode='+addressPostCode+
  '&contactNo='+contactNo+'&clientEmail='+clientEmail+
  '&emergencyContactName='+emergencyContactName+
  '&emergencyContactNo='+emergencyContactNo+'&typeOfWork='+typeOfWork+
  '&qualification='+qualification+'&ni='+ni+'&dob='+dob+
  '&workingVia='+workingVia+'&nameOfComapny='+nameOfComapny+
  '&workInUK='+workInUK+'&license='+license+'&convictions='+convictions+
  '&convTextArea='+convTextArea+'&disability='+disability+
  '&disaTextArea='+disaTextArea+'&ref1Name='+ref1Name+
  '&ref1Title='+ref1Title+'&ref1Company='+ref1Company+
  '&ref1Address='+ref1Address+'&ref1Number='+ref1Number+
  '&ref1Email='+ref1Email+'&ref2Name='+ref2Name+'&ref2Title='+ref2Title+
  '&ref2Company='+ref2Company+'&ref2Address='+ref2Address+
  '&ref2Number='+ref2Number+'&ref2Email='+ref2Email+'&dataURL='+dataURL;
  var xhttp = new XMLHttpRequest();
  xhttp.open("POST",'/Form',true);
  xhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
  xhttp. onreadystatechange = function() {
        if (xhttp.readyState === 4 && xhttp.status === 200) {
           console.log(xhttp.responseText);
            document.getElementById("txt").innerHTML = xhttp.responseText;
          } else {
            console.error(xhttp.statusText);
          }
       };

  xhttp.send(params);
  return false;
}
