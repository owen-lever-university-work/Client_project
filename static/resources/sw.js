importScripts('/static/resources/cache-polyfill.js');

self.addEventListener('install', function(e) {
 e.waitUntil(
   caches.open('university').then(function(cache) {
     return cache.addAll([
       '/home',
       '/admin',
       '/adminViewDetails',
       '/adminViewAddress',
       '/adminViewReferences',
       '/adminViewWork',
       '/adminViewDocuments',
       '/adminViewAll',
       '/adminDelete',
       '/adminLogins',
       '/login',
       '/signout',
       '/signup',
       '/form',
       '/static/styles/main.css',
       '/static/resources/Acorn.png',
       '/static/resources/fabric.min.js',
       '/static/resources/scripts.js',
       '/static/resources/signature.js',
       '/static/resources/validate.js'
     ]);
   })
 );
});

self.addEventListener('fetch', function(event) {
  console.log(event.request.url);
  event.respondWith(
    caches.match(event.request).then(function(response) {
      return response || fetch(event.request);
    })
  );
});
