function checkNull(form, field){

  if ((document.forms[form][field].value).length < 1){
    document.getElementsByName(field)[0].classList.add('Incomplete');
    pass = false;
  }else{
    document.getElementsByName(field)[0].classList.remove('Incomplete');
 };
}

function validate(){
  pass = true;
  checkNull("Registration", "clientName");
  checkNull("Registration", "surname");
  checkNull("Registration", "address1");
  checkNull("Registration", "addressCity");
  checkNull("Registration", "addressCountry");
  checkNull("Registration", "addressPostCode");
  checkNull("Registration", "contactNo");
  checkNull("Registration", "clientEmail");
  checkNull("Registration", "emergencyContactName");
  checkNull("Registration", "emergencyContactNo");
  checkNull("Registration", "typeOfWork");
  checkNull("Registration", "qualification");

  checkNull("Registration", "dob");
  checkNull("Registration", "nameOfCompany");

  checkNull("Registration", "ref1Name");
  checkNull("Registration", "ref1Title");
  checkNull("Registration", "ref1Company");
  checkNull("Registration", "ref1Address");
  checkNull("Registration", "ref1Number");
  checkNull("Registration", "ref1Email");

  checkNull("Registration", "ref2Name");
  checkNull("Registration", "ref2Title");
  checkNull("Registration", "ref2Company");
  checkNull("Registration", "ref2Address");
  checkNull("Registration", "ref2Number");
  checkNull("Registration", "ref2Email");
  if(document.forms["Registration"]["convictions"].value == "yes"){
    checkNull("Registration", "convTextArea")
  }

  if(document.forms["Registration"]["disability"].value == "yes"){
    checkNull("Registration", "disaTextArea")
  }

  if ((document.forms["Registration"]["ni"].value).length >0){
    // Regex code adapted from https://www.braemoor.co.uk/software/nino.shtml
    var exp1 = /^[A-CEGHJ-NOPR-TW-Z]{1}[A-CEGHJ-NPR-TW-Z]{1}[0-9]{6}[A-D\s]{1}/i;
    var exp2 = /(^GB)|(^BG)|(^NK)|(^KN)|(^TN)|(^NT)|(^ZZ).+/i;
    if ((exp1.test(document.forms["Registration"]["ni"].value)) && !(exp2.test(document.forms["Registration"]["ni"].value))){
      document.getElementsByName("ni")[0].classList.remove('Incomplete');
    }else{
      document.getElementsByName("ni")[0].classList.add('Incomplete');
    };
  };

  if(pass===true){
    sendSignature();
    sendData()
  }

  return pass;
  }

// function sendData(){
//   fieldnames = ["'clientName'","'Surname'","'address1'","'address2'","'addressCity'","'addressCountry'",
//   "'addressPostCode'","'contactNo'","'clientEmail'","'emergencyContactName'","'emergencyContactNo'",
//   "'typeOfWork'","'qualification'","'ni'","'dob'","'workingVia'","'nameOfCompany'","'workInUK'",
//   "'license'","'convictions'","'convTextArea'","'disability'","'disaTextArea'","'ref1Name'","'ref1Company'",
//   "'ref1Address'","'ref1Number'","'ref1Email'","'ref2Name'","'ref2Company'","'ref2Address'","'ref2Number'",
//   "'ref2Email'"]
//
//   params = ""
//   for(i in fieldnames){
//     // fieldvalues.push(document.forms["Registration"][i].value)
//     params += i+"="+document.forms["Registration"][i].value+"&";
//   }
//
//
//   // params = 'name='+name+'&description='+description+'&credits='+credits;
//
//
//   var xhttp = new XMLHttpRequest();
//   xhttp.open("POST", '/form', true); // true is asynchronous
//   xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
//   xhttp.onload = function() {
//     if (xhttp.readyState === 4 && xhttp.status === 200) {
//       console.log(xhttp.responseText);
//       document.getElementById("txt").innerHTML = xhttp.responseText;
//     } else {
//       console.error(xhttp.statusText);
//     }
//   };
//   xhttp.send(params);
//   return false;
// }
function checkSignUp(){
  pass = true;
  checkNull("signUp","user_name")
  checkNull("signUp","user_email")
  checkNull("signUp","user_email_check")
  checkNull("signUp","user_password")
  checkNull("signUp","user_password_check")



  email = document.getElementById("signUp").elements.namedItem("user_email").value;
  email_check = document.getElementById("signUp").elements.namedItem("user_email_check").value;
  password = document.getElementById("signUp").elements.namedItem("user_password").value;
  password_check = document.getElementById("signUp").elements.namedItem("user_password_check").value;


  if(email!=email_check){
    document.getElementById("match_email").innerHTML = "<p class=warning>Emails do not match</p>";
    pass = false;
  }else{
    document.getElementById("match_email").innerHTML = ""
 };
 if(password!=password_check){
   document.getElementById("match_password").innerHTML = "<p class=warning>passwords do not match</p>";
   pass = false;
 }else{
   document.getElementById("match_password").innerHTML = ""
};
 return pass;

  }

  // function sendSignUp(){
  //   fieldnames = ["'user_name'","'user_email'","'user_password'"]
  //
  //   params = ""
  //   for(i in fieldnames){
  //     // fieldvalues.push(document.forms["Registration"][i].value)
  //     params += i+"="+document.forms["signUp"][i].value+"&";
  //   }
  //
  //
  //   // params = 'name='+name+'&description='+description+'&credits='+credits;
  //
  //
  //   var xhttp = new XMLHttpRequest();
  //   xhttp.open("POST", '/signup', true); // true is asynchronous
  //   xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  //   xhttp.onload = function() {
  //     if (xhttp.readyState === 4 && xhttp.status === 200) {
  //       console.log(xhttp.responseText);
  //       document.getElementById("txt").innerHTML = xhttp.responseText;
  //     } else {
  //       console.error(xhttp.statusText);
  //     }
  //   };
  //   xhttp.send(params);
  //   return false;
  // }
  // }
