var canvas = new fabric.Canvas('canvas',{
  isDrawingMode: true
});
var userScreenWidth = window.innerWidth;// * window.devicePixelRatio;
var canvasWidth = (userScreenWidth)*0.44;
var canvasWidthMob = (userScreenWidth)*0.95;
if (userScreenWidth>799)
	{
	  fabric.isTouchSupported = true;
	  canvas.freeDrawingBrush.color = "black";
	  canvas.freeDrawingBrush.width = 5;
	  canvas.setHeight(200);
	  canvas.setWidth(canvasWidth);
	  canvas.renderAll();
	}
else
	{
	  fabric.isTouchSupported = true;
	  canvas.freeDrawingBrush.color = "black";
	  canvas.freeDrawingBrush.width = 5;
	  canvas.setHeight(200);
	  canvas.setWidth(canvasWidthMob);
	  canvas.renderAll();
	}
function clearCanvas()
  {
    canvas.clear()
  }

function extraInfoConviction(){
    if (document.getElementById("convictionsYes").selected)
      {
        document.getElementById("hasConviction").style.display = "inline";
      }else{
        document.getElementById("hasConviction").style.display = "none";
      }
  }
function extraInfoDisability(){
    if (document.getElementById("disabilitiesYes").selected)
      {
        document.getElementById("hasDisability").style.display = "inline";
      }else{
        document.getElementById("hasDisability").style.display = "none";
      }
  }
